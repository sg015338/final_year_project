<?php
include 'header.php';
?>

<div class="row">
  <div class="large-12 columns">
    <h2>Curriculum Overview Dashboard</h2>
    <p>Welcome to the Web-Based Computer Science Curriculum Analytics Platform. The Curriculum Overview Dashboard provides insights into module participation, popularity, credits, delivery methods, and more. Use the filters provided to customize the view and analyze specific data points.</p>
  </div>
</div>

<div class="row">
  <div class="large-12 columns">
    <h3>Sheets included in Curriculum Overview:</h3>
    <ol>
      <li>
        <h4>Module participation across years</h4>
        <p>This sheet displays the total number of modules participated over time using a line graph, with data colored to help users distinguish between years of higher and lower values. It helps users, including university-level CQSD, department management teams, lecturers, and students, to identify trends in module participation, informing decisions about module offerings, resource allocation, and curriculum adjustments.</p>
      </li>
      <li>
        <h4>Most popular modules</h4>
        <p>This sheet shows the number of students enrolled in a module over time using a heatmap. Squares are labeled with the module code and colored and sized to help users distinguish between modules of higher and lower student numbers. Understanding module popularity is valuable for decision-making regarding resource allocation, curriculum adjustments, and potential areas for improvement or expansion.</p>
      </li>
      <li>
        <h4>Number of module participation across terms</h4>
        <p>This sheet displays the number of modules enrolled in and the relative terms in which they operate using a bar chart, colored to help users distinguish between terms with higher and lower student numbers. It provides insight into module participation across different terms, enabling informed decisions about scheduling, resource allocation, and curriculum planning.</p>
      </li>
      <li>
        <h4>Modules with a large number of credits</h4>
        <p>This sheet shows the number of credits a module is worth using a heatmap, colored and sized to help users distinguish between modules of higher and lower credit amounts. Understanding credit distribution is essential for curriculum planning and resource allocation, ensuring a balanced curriculum and identifying potential areas for adjustment or improvement.</p>
      </li>
      <li>
        <h4>Delivery method and corresponding percentage of students</h4>
        <p>This sheet displays students' most popular delivery methods for modules using a pie chart, colored by delivery method type and labeled by delivery method and percentage. Identifying popular delivery methods helps stakeholders make informed decisions about teaching methods, resource allocation, and potential areas for innovation or improvement.</p>
      </li>
      <li>
        <h4>Module Weight Comparison</h4>
        <p>This sheet shows the assessment types of modules and their weighting using a bar chart, colored by assessment type. It offers valuable information for university-level CQSD, department management teams, lecturers, and students, ensuring a balanced and fair evaluation process, identifying potential areas for improvement, and providing students with a clear understanding of their academic requirements.</p>
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="large-12 columns">
    <h3>Sheets included in Student Performance and Demographics Dashboard:</h3>
    <ol>
      <li>
        <h4>Average score by academic year and part name</h4>
        <p>This sheet displays the average grades for each Part (Year 1/2/3) for each year using a bar chart, with bars colored according to average grade and labeled with their values. It helps stakeholders track student performance, identify trends, areas requiring additional support, and evaluate the effectiveness of teaching methods and curriculum content.</p>
      </li>
      <li>
        <h4>Grades Breakdown by students</h4>
        <p>This sheet shows the percentage of students and the grade they receive for each year using a bar chart. Bars are colored according to the percentage of students receiving a grade and labeled with their percentage. It enables stakeholders to understand the distribution of grades, identify areas for improvement, ensure fair evaluation processes, and track the overall success of students in the program.</p>
      </li>
      <li>
        <h4>Percentage distribution of students by nationality</h4>
        <p>This sheet displays the percentage of students from various countries using a geographical map, with countries colored according to the percentage of students from that country and labeled with the percentage. It helps stakeholders make informed decisions about internationalization strategies, student support services, and the development of inclusive and culturally sensitive curricula.</p>
      </li>
      <li>
        <h4>Dominant assessment types</h4>
        <p>This sheet shows the percentage of frequency of assessment types using a pie chart, with different assessment types colored and their percentages labeled. It helps stakeholders ensure a balanced and varied evaluation process, identify potential areas for improvement or innovation, and cater to different learning styles and preferences.</p>
      </li>
      <li>
        <h4>Gender distribution among students</h4>
        <p>This sheet displays the percentage of genders of students using a pie chart, with different genders colored and their percentages labeled. It enables stakeholders to identify potential areas for improvement regarding gender inclusivity and equality and make informed decisions about recruitment and support strategies.</p>
      </li>
      <li>
        <h4>ModulesGradesYear</h4>
        <p>This sheet shows the percentage of students that received a grade for each module, per module, per year using a stacked bar chart, with different grades colored. It helps stakeholders identify trends in student performance, evaluate the effectiveness of teaching methods, and pinpoint modules that may require additional support or revision.</p>
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="large-12 columns">
    <h3>Sheets included in Recommendations Dashboard:</h3>
    <ol>
      <li>
        <h4>Stats Board</h4>
        <p>This sheet shows average score, number of students, and number of records currently under review, with filters applied. Visualized through text boxes at the top of the page that change with input, it provides a high-level overview for users to grasp the general context before diving into more detailed visualizations.</p>
      </li>
      <li>
        <h4>Metric Breakdown - Variable and Sub variable</h4>
        <p>This sheet has additional filters for Variable, Subvariable, and Measure to sort by. It shows average score, number of students, number of records, trending on average, and assessments taken. Visualized with 3 bar charts in a row and a line graph over years, it helps users identify patterns and trends across different variables and subvariables, allowing stakeholders to pinpoint areas that may require attention or improvement.</p>
      </li>
      <li>
        <h4>Distribution of Average Score by Variable</h4>
        <p>This sheet has a dropdown box to select a variable and shows the scores and their distribution. Visualized using box plots of modules presented side by side with data points color-coded by variable, it allows users to compare the distribution of average scores across different variables, helping identify any disparities or outliers that may warrant further investigation.</p>
      </li>
      <li>
        <h4>Trending on Average Score by Variable</h4>
        <p>This sheet features a dropdown box to select a variable and shows the trends of variable modules across the years. Visualized using line graphs of variables layered on top of each other with data points and lines color-coded by variable, it enables users to spot trends and changes in performance, helping stakeholders identify potential areas of improvement and track the impact of implemented changes.</p>
      </li>
      <li>
        <h4>Details</h4>
        <p>This sheet displays detailed information about students, their academic years, assessment types, module code, module title, and score. Visualized as a table, it provides granular information for all stakeholders to analyze individual data points and identify specific issues or trends, aiding in making informed decisions about grading, feedback, and resource allocation.</p>
      </li>
    </ol>
  </div>
</div>

<?php
include 'footer.php';
?>