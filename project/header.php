<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EduTrack</title>
    <link href="/project/stylesheets/normalize.css" rel="stylesheet" type="text/css">
    <link href="/project/stylesheets/all.css" rel="stylesheet" type="text/css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/3.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/project/images/favicon.png" rel="icon" type="image/png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.0/css/all.min.css">
</head>
<style>
    .tableau-wrapper {
        max-width: 90%;
        margin: 0 auto;
    }
</style>
<body class="index">
    <header class="header contain-to-grid">
        <nav class="top-bar" data-topbar>
            <ul class="title-area">
                <li class="name">
                    <h1><a href="/project/index.php">Home</a></h1>
                </li>
                <li class="toggle-topbar menu-icon">
                    <a href="#">
                        <span>Menu</span>
                    </a>
                </li>
            </ul>

            <section class="top-bar-section">
                <!-- Left Nav Section -->
                <ul class="left">
                    <li class="item"><a href="/project/curriculum_overview.php">Curriculum Overview</a></li>
                    <li class="item"><a href="/project/student_metric.php">Student Metrics</a></li>
                    <li class="item"><a href="/project/recommendations.php">Recommendations</a></li>
                </ul>
            </section>
        </nav>
    </header>

    <div class="wrapper">
        <div class="hero">
            <div class="row">
                <div class="large-12 columns">
                    <h1><span>EduTrack: Computer Science Curriculum</span></h1>
                </div>
            </div>
        </div>
